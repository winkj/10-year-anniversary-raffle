What dev tools are you using to integrate with Bitbucket?
  I use plain git on the command line

What dev tools would you like to see integrated with Bitbucket?
  I'm pretty happy with the current state :)

What feature would you like to have improved the most on Bitbucket?
  a way to add inline comments on a particular line of code of a changeset/pull
  request would be great

For your free Bitbucket t-shirt, what size will you need?
  M
